"""@package points

This class just wraps two objects so they can be reffered to as
the x and y attributs of another.
"""

"""Wraps two objects

This is so that ehy can be referred to as the x and y attributes of another.
"""
class Point:
	"""The constructor"""
	def __init__(self, x, y):
		self.x = x
		self.y = y
	"""Returns the string representation of this Point object.

	This will be formatted as (x,y).
	The objects must implement the str(self) method.
	"""
	def __str__(self):
		return str(self.x) + " " + str(self.y)
