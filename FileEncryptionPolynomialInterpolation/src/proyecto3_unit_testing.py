"""@package proyecto3_unit_testing

Provée pruebas unitarias para proyecto 3.
"""
from random import randint
import unittest
import zp_field
import polynomials
from points import Point

class ZpFieldTestCase(unittest.TestCase):
	def setUp(self):
		self.number = zp_field.ZpNumber(randint(1,zp_field.p))
		zero   = zp_field.additive_identity
		self.one    = zp_field.multiplicative_identity
	def test_multiplicative_inverse(self):
		inverse = zp_field.calculate_multiplicative_inverse(self.number)
		self.assertEqual(self.number * inverse, self.one)
	def test_modular_exponentation(self):
		# 100 para que no se tarde la prueba a patín.
		exponent = zp_field.ZpNumber(randint(1,100))
		pow1   = zp_field.modular_power(self.number, exponent)
		pow2   = self.number
		for i in range(0, exponent.number-1):
			pow2 = pow2 * self.number
		self.assertEqual(pow1, pow2)

class PolynomialsTestCase(unittest.TestCase):
	def setUp(self):
		degree = randint(1,100)
		coefficients = []
		for i in range(degree+1):
			coefficients.append(zp_field.ZpNumber(randint(1,zp_field.p)))
		self.polynomial = polynomials.Polynomial(coefficients)
		self.zero = zp_field.additive_identity
		self.one  = zp_field.multiplicative_identity
	def test_interpolation(self):
		x = zp_field.ZpNumber(randint(0,zp_field.p))
		y = self.polynomial.evaluate_at(self.zero,self.zero)
		
		# The points must be unique
		points = []
		used = set()
		for i in range(self.polynomial.get_degree()):
			# Arbitrary
			a = randint(0, zp_field.p)
			if a not in used:
				x = zp_field.ZpNumber(a)
				y = self.polynomial.evaluate_at(x, self.zero)
				points.append(Point(x,y))
				used.add(a)
		
		y2 = polynomials.calculate_interpolation_polynomial(x, 
		                                                    points,
															self.zero,
															self.one)
		self.assertEqual(y, y2)

if __name__ == '__main__':
	suite = unittest.TestLoader().loadTestsFromTestCase(PolynomialsTestCase)
	unittest.TextTestRunner(verbosity=3).run(suite)
	suite = unittest.TestLoader().loadTestsFromTestCase(ZpFieldTestCase)
	unittest.TextTestRunner(verbosity=3).run(suite)
