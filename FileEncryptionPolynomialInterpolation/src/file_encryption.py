"""@package file_encryption

This package implements the Shamir Sharin Scheme using
the Zp finite field.
"""
from . import polynomials
from . import zp_field
from .points              import Point
from random               import randint
from .zp_field            import ZpNumber
from hashlib              import sha256
from getpass              import getpass
from Crypto.Cipher        import AES
from Crypto               import Random
from Crypto.Util.number   import long_to_bytes
from Crypto.Util.number   import bytes_to_long

def build_random_polynomial_with_key(degree, key):
	"""Returns a random polynomial with the key as the independent coefficient.

	The degree of the polynomial must be a 256 bit number.
	"""
	coefficients = []
	for i in range(degree):
		coefficients.append(ZpNumber(randint(1,zp_field.p)))
	coefficients.append(ZpNumber(key))
	return polynomials.Polynomial(coefficients)

def get_shamir_keys(number_of_keys, polynomial):
	"""Returns a set of keys as defined in the Shamir Sharing Scheme

	This will be O(n) where n is the number of keys.
	"""
	additive_identity       = zp_field.additive_identity
	multiplicative_identity = zp_field.multiplicative_identity
	# The keys must be unique
	keys = []
	used = set()
	while len(keys) < (number_of_keys):
		# Arbitrary
		a = randint(0, zp_field.p)
		if a not in used:
			x = ZpNumber(a)
			y = polynomial.evaluate_at(x, additive_identity)
			keys.append(Point(x,y))
			used.add(a)
	return keys

def aes_encrypt(message, key):
	"""Encrypts a message using the AES algorithm with a given key.

	The key must be a multiple of 32 bits in length.
	"""
	# Initialization vector
	iv      = Random.new().read(AES.block_size)
	cypher  = AES.new(key, AES.MODE_CBC, iv)
	# Pad message
	message = message + b"\0"*(AES.block_size - len(message)%AES.block_size)
	return iv + cypher.encrypt(message)

def aes_decrypt(encrypted_message, key):
	"""Decrypts a message using the AES algorithm with a given key.

	The key must be a multiple of 32 bits in length.
	"""
	# Retrieving initialization vector
	iv                = encrypted_message[:AES.block_size]
	cypher            = AES.new(key,AES.MODE_CBC, iv)
	decrypted_message = cypher.decrypt(encrypted_message[AES.block_size:])
	return decrypted_message.rstrip(b"\0")

def calculate_key(keys):
	"""Returns the original key from the set of keys

	This will be O(n^2) where n is the number of keys.
	"""
	additive_identity       = zp_field.additive_identity
	multiplicative_identity = zp_field.multiplicative_identity
	return polynomials.calculate_interpolation_polynomial(ZpNumber(0), keys,
	                                                     additive_identity,
                                                         multiplicative_identity)

def encrypt(original_filename, n, t, keys_filename):
	"""Encrypts a file using the Shamir Sharing Scheme

	The encrypted file and the file that contains the keys will be
	stored in the current directory.
	"""
	number_of_keys = int(n)
	treshold       = int(t)
	if number_of_keys < treshold or number_of_keys < 1 or treshold < 1:
		raise ValueError
	password       = getpass()
	key_hash   = sha256(password.encode('utf-8'))
	key        = ZpNumber(bytes_to_long(key_hash.digest())).number
	key_bytes  = long_to_bytes(key, blocksize=32)
	polynomial = build_random_polynomial_with_key(treshold-1,key)
	keys       = get_shamir_keys(number_of_keys, polynomial)
	with open(keys_filename + '.keys', 'w', encoding='utf8') as keys_file:
		for point in keys:
			keys_file.write(str(point) + '\n')
	with open(original_filename, 'rb') as original_file:
		encrypted_message = aes_encrypt(original_file.read(), key_bytes)
	with open(keys_filename + '.aes', 'wb') as encrypted_file:
		padded_filename = original_filename.encode("utf8")
		padded_filename += b"\0"*(128 - len(original_filename)%128)
		start = padded_filename + long_to_bytes(t, blocksize = 32)
		encrypted_file.write(start + encrypted_message)

def decrypt(keys_filename, encrypted_filename):
	"""Decrypts a file that was encrypted using the Shamir Sharing Scheme

	The decrypted file will be stored in the current directory with its
	original filename.
	"""
	with open(keys_filename, 'r', encoding='utf8') as f:
		keys = []
		for i,key in enumerate(f.read().splitlines()):
			x = ZpNumber(int(key.split()[0]))
			y = ZpNumber(int(key.split()[1]))
			keys.append(Point(x,y))
	with open(encrypted_filename, 'rb') as encrypted_file:
		original_filename = encrypted_file.read(128).rstrip(b"\0").decode("utf-8")
		t = bytes_to_long(encrypted_file.read(32))
		# Key es un elemento de Zp, cabe en 32 bytes
		key = calculate_key(keys[:t])
		key_bytes = long_to_bytes(key.number, blocksize=32)
		decrypted_message = aes_decrypt(encrypted_file.read(), key_bytes)
	with open(original_filename, 'wb') as decrypted_file:
		decrypted_file.write(decrypted_message)
