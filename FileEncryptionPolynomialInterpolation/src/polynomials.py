"""@package polynomials
Provides polynomial related general-use methods.

Any field can be used, but the additive neutral and the 
multiplicative identity must be indicated.
That is, coefficients and numbers can be of any class but
those classes must implement the standard field operations
"""

def calculate_interpolation_polynomial(x, points, additive_identity,
                                       multiplicative_identity):
	"""Interpolation polynomial in the Lagrange form 

	Given k points, this method will interpolate the k-1 degree polynomial
	which satisfies those k contraints.
	No two points must be identical, nor must a single element in the
	domain be related to more than one value in the image.
	The points must implement x and y attributes.

	This will be O(k^2)
	"""
	addition = additive_identity
	for j in range(len(points)):
		addition += points[j].y * get_lagrange_basis_polinomial(j, x, points,
		                                              multiplicative_identity)
	return addition

def get_lagrange_basis_polinomial(j, x, points, multiplicative_identity):
	"""Returns the j-th element of the Lagrange basis for the given points

	No two points must be identical, nor must a single element in the
	domain be related to more than one value in the image.
	"""
	# j'th polynomial of the Lagrange basis polynomials
	product = multiplicative_identity
	for m in range(len(points)):
		if m != j:
			product = product * ((x - points[m].x)/(points[j].x - points[m].x))
	return product

class Polynomial:
	"""Models a polynomial over a field.

	The coefficients can be objects of any class.
	"""
	def __str__(self):
		"""Returns the string representation of the polynomial.

		This will be in the form: a_nx^n + a_n-1x^n-1 + ... + a_0
		"""
		polynomial = ""
		degree = len(self.coefficients)-1
		for n, coefficient in list(enumerate(self.coefficients)):
			polynomial += str(coefficient) + "x^" + str(degree-n)
			if n < degree:
				polynomial += " + "
		return polynomial
	def __init__(self, coefficients):
		"""Builds a polynomial given a list of coefficients

		The coefficients must be objects that implement the standard field
		operations.
		"""
		# Coefficients from x^n to x^0
		self.coefficients = coefficients
	def get_degree(self):
		return len(self.coefficients)
	def evaluate_at(self, x, additive_identity):
		"""Evaluates a polynomial in a given point

		This will be O(n) where n is the degree of the polynomial.
		"""
		# Using Horner's algorithm
		result = additive_identity
		for coefficient in self.coefficients:
			result = (result * x) + coefficient
		return result
