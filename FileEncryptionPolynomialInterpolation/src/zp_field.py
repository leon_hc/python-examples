"""@package zp_field

Models the finite field Zp using modular arithmetic.

The prime p is large enough for cryptography (257 bits).
In particular, every number x will model an residue equivalence class
modulo p, so x is in [0,p).
"""
p = 208351617316091241234326746312124448251235562226470491514186331217050270460481

class ZpNumber:
	def __eq__(self, value):
		"""This method compares two ZpNumbers
		
		This overrides the == python operator.
		"""
		return self.number == value.number
	def __mul__(self, value):
		"""The multiplication as defined in Zp.

		This overrides the * python operator.
		"""
		return ZpNumber((self.number * value.number) % p)
	def __add__(self, value):
		"""The addition as defined in Zp.

		This overrides the + python operator.
		"""
		return ZpNumber((self.number + value.number) % p)
	def __sub__(self, value):
		"""The substraction as defined in Zp.

		This overrides the - python operator.
		"""
		return ZpNumber((self.number - value.number) % p)
	def __truediv__(self, value):
		"""The division as defined in Zp.
		
		That is a/b = a*b^-1.

		This overrides the / python operator.
		"""
		return self * calculate_multiplicative_inverse(value)
	def __init__(self, number):
		"""Given an integer, builds an element of Zp.

		This will be modulo p.
		"""
		self.number = number % p
	def __str__(self):
		"""Returns the string representation of the ZpNumber

		This is just the number that represents the class that the ZpNumber
		represents.
		"""
		return str(self.number)

def calculate_multiplicative_inverse(n):
	"""Returns the multiplicative inverse of a number in the field.

	This will be O(log(n)) where n is the prime number.
	"""
	return modular_power(n, ZpNumber(p-2))
	
def modular_power(a, b):
	"""Modular fast exponentiation

	This will be O(log(n)) where n is the power.
	"""
	a = a.number
	b = b.number
	return ZpNumber(modular_power_aux(a, b))
def modular_power_aux(a, b):
	if b == 1:
		return a
	if b == 0:
		return 1
	if b % 2 == 1:
		return (a * modular_power_aux(a, b-1)) % p
	else:
		descenso = modular_power_aux(a,b//2)
		return (descenso*descenso) % p

additive_identity       = ZpNumber(0)
multiplicative_identity = ZpNumber(1)
