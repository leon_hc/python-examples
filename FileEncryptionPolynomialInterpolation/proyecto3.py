"""!@mainpage Proyecto3 - Modelado y programación

@section intro Overview
This program is an assigment for the Modelado y Programación course at the U.N.A.M's
Facultad de Ciencias; as imparted by Dr. José de Jesús Galaviz Casas in 2016-1.
The purpose of the program is to implement the Shamir Secret Sharing Scheme
using finite-field arithmetic.

@section usage Usage
The programm can be run as one of the following:
	proyecto3 -c file n t output_filename     This will encrypt file and
	                                          write n keys to 
											  output_filename.keys
											  and an encrypted version of
											  file as output_filename.aes
	proyecto3 -d file keys_file               This will decrypt file using
										      keys_file and will write
											  the decrypted file with its
											  original name.
											  

"""

"""@package proyecto3

This package serves as the main program for Proyecto3
This package must not be imported and must be run from the command line.

"""
import argparse
from src import file_encryption
import sys

"""Provides a convinient way to print messages to the standard error stream.

The messages will be formatted as "Error: message"
"""
def print_error(message):
	print("Error: " + message, file=sys.stderr)

parser = argparse.ArgumentParser()
parser.add_argument("-c", nargs=4, help="Opción para encriptar")
parser.add_argument("-d", nargs=2, help="Opción para desencriptar")

args = parser.parse_args()

if args.c:
	try:
		file_encryption.encrypt(args.c[0],args.c[1],args.c[2],args.c[3])
		print("Encriptado en " + args.c[3] + ".aes. Llaves guardadas en: " + args.c[3] + ".keys")
	except OSError as exception:
		print_error(exception.filename + " no se pudo leer o escribir.")
	except ValueError as exception:
		print_error("Se intentaron generar menos llaves de las que se necesitan")
elif args.d:
	try:
		file_encryption.decrypt(args.d[0],args.d[1])
		print("Archivo desencriptado correctamente")
	except OSError as exception:
		print_error(exception.filename + " no se pudo leer o escribir.")
else:
	parser.print_help()
